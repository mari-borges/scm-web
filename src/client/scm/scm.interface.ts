export interface IScm{
    
    id?: string,
    nmCompleto: String;
    dtNascimento: Date;
    idEndereco: number;
    nmCidade: String;
    nmEstado: String;
    idCpf: number;
    idRg: number;
    dsEspecialidade: string,
    dsSintomas: string,
    dsEmail: string
}