import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { ScmListagemComponent } from './scm-listagem/scm-listagem.component';
// import { ScmDetalheComponent } from './scm-detalhe/scm-detalhe.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { ScmCadastroComponent } from './scm-cadastro/scm-cadastro.component';



const routes = [
  {
    path: 'cadastro',
    component: ScmCadastroComponent,
},

];

@NgModule({
  declarations: [
   ScmCadastroComponent

  ],
  imports: [
    CommonModule,

    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    HttpClientModule
  ]
})
export class ScmModule { }
