import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmListagemComponent } from './scm-listagem.component';

describe('ScmListagemComponent', () => {
  let component: ScmListagemComponent;
  let fixture: ComponentFixture<ScmListagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmListagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
