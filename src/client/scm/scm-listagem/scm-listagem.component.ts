import { IScm } from '../scm.interface';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { empty, Observable, Subject, EMPTY  } from 'rxjs';

import { catchError, switchMap, take } from 'rxjs/operators';
import { MenssageService } from '../../util/menssage.service';
import { ScmService } from 'src/client/util/scm.service';

@Component({
  selector: 'app-scm-listagem',
  templateUrl: './scm-listagem.component.html',
  styleUrls: ['./scm-listagem.component.scss']
})
export class ScmListagemComponent implements OnInit {
  [x: string]: any;

  clients$: Observable<IScm[]> | undefined;

  client!: IScm;
  error$ = new Subject<boolean>();
  
  
  @ViewChild('deleteModal', { static: true }) deleteModal: any;
 
  constructor(
    private readonly _router: Router,
    private readonly _scmservice: ScmService,
    private readonly route: ActivatedRoute,
    private readonly _menssageService: MenssageService,

  ) { }

  ngOnInit(): void {

  //  this._scmservice.list().subscribe(dados => this.clients$ = dados);
  this.onRefresh();
  }
  
  onRefresh() {
    this.clients$ = this._scmservice.list().pipe();
     
  }
  onEdit(id: any) {
    this._router.navigate(['editar', id], { relativeTo: this.route });
  }
    
  async delete(): Promise<any> {
    const canDelete = await this._menssageService.confirmDelete();
    if (canDelete) {
      this.canDelete();
    }
  }

  canDelete(): void {
    this._scmservice
      .delete(this.clientSelecte.id)
      .pipe()
      .subscribe(
        () => {
          this.deleteData();
          this._menssageService.deleteSuccess();
        },
        () => this._menssageService.errorRest()
      );
  }

}
