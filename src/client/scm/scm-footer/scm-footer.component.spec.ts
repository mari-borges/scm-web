import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmFooterComponent } from './scm-footer.component';

describe('ScmFooterComponent', () => {
  let component: ScmFooterComponent;
  let fixture: ComponentFixture<ScmFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
