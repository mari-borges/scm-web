import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmDetalheComponent } from './scm-detalhe.component';

describe('ScmDetalheComponent', () => {
  let component: ScmDetalheComponent;
  let fixture: ComponentFixture<ScmDetalheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmDetalheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
