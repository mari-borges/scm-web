import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmHeaderComponent } from './scm-header.component';

describe('ScmHeaderComponent', () => {
  let component: ScmHeaderComponent;
  let fixture: ComponentFixture<ScmHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
