import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmCadastroComponent } from './scm-cadastro.component';

describe('ScmCadastroComponent', () => {
  let component: ScmCadastroComponent;
  let fixture: ComponentFixture<ScmCadastroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmCadastroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
