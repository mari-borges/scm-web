import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {BehaviorSubject, Observable, Subject } from "rxjs";
import { error } from 'protractor';
import { ScmService } from 'src/client/util/scm.service';


@Component({
  selector: 'app-scm-cadastro',
  templateUrl: './scm-cadastro.component.html',
  styleUrls: ['./scm-cadastro.component.css']
})
export class ScmCadastroComponent implements OnInit {
  title = 'scm-web';

  form!: FormGroup;
  submitted!: boolean;
  isFormEdit!: boolean;
  selected:any;
  filtered :any;
  

  private _unsubscribeAll: Subject<any>;
  constructor(
    private readonly _formBuider: FormBuilder,
    private readonly _scmService: ScmService,
    private readonly route: ActivatedRoute 
    

  ){
    this._unsubscribeAll = new Subject();
    
    
   }

   public especialidade = [
    "Cardiologia",
    "Ortopedia",
    "Pediatria",
    "Neurologia",
    "Cirurgia Geral",
  ];

  ngOnInit(): void {

    this.route.params.subscribe(
    (params: any) => {
      const id = params['id'];
      console.log(id)
      const client$ = this._scmService.loadByID(id);
      client$.subscribe(client => {
        this.updateForm(client)

      });
    }
    );

  this.form = this._formBuider.group ({
    id: [null],
    nmCompleto: [null, [Validators.required]],
    dtNascimento: [null, [Validators.required]],
    idEndereco: [" ", [Validators.required]],
    dsCidade: [" ", [Validators.required]],
    dsEstado: [" ", [Validators.required]],
    idCpf: [" ", [Validators.required]],
    idRg: [" ", [Validators.required]],
    dsEmail: [" ", [Validators.required,Validators.email,]],
    dsEspecialidade: [" ", [ Validators.required]],
    dsSintomas: [" ", [ Validators.required]]
  
  });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.form.value)
    if (!this.form.valid){
      console.log('submit');
      if(this.form.value.id){
         this._scmService.update(this.form.value).subscribe(

         )
      }else {
        this._scmService.create(this.form.value).subscribe(
          success => console.log('sucesso'),
          error => console.error(error),
          () => console.log('requece sucesso')
        );
       }}
  }
  updateForm(client: any) {
    this.form.patchValue({
      id: client.id,
      nmCompleto: client.nmCompleto,
      dtNascimento: client.dtNascimento,
      idEndereco: client.idEndereco,
      dsCidade: client.dsCidade,
      dsEstado: client.dsEstado,
      idCpf: client.idCpf,
      idRg: client.idRg,
      dsEspecialidade: client.dsEspecialidade,
      dsSintomas: client.dsSintomas

    })
  } 
   

     




}
