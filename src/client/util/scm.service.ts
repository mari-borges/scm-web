
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, pipe, ReplaySubject, Subject } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { orderBy, take } from 'lodash';
import { tap, delay } from 'rxjs/operators';
import { IScm } from '../scm/scm.interface';

@Injectable({
  providedIn: 'root'
})
export class ScmService {

  private readonly API = `${environment.API}scm`;

  private scm$: BehaviorSubject<IScm[]> = new BehaviorSubject<IScm[]>([]);
 


  constructor(
    private readonly _http: HttpClient 
  ){}

  create(base: any){
    return this._http.post(this.API, base).pipe();

  }
  
  loadByID(id: IScm) {
    return this._http.get(`${this.API}/${id}`).pipe();
  }

  update(client: IScm): Observable<IScm> | any {
    return this._http.put(`${this.API}/${client.id}`, client).pipe();

  }
  list(){
    return this._http.get<IScm[]>(this.API)
      .pipe(
        delay(2000),
        tap(console.log)
    );
  }
  setValues(values: IScm[]): void {
    if (values.length > 0) {
      this.scm$.next(values);
    }
  }

  getformValues(): any {
		return this.scm$.getValue();
  }
  delete(id: number) {
    return this._http.delete(`${this.API}/${id}`).pipe();
    
  }
  salve(client: any){
    if(client.id){
      return this.update(client);
    }
    return this.create(client);
  }
}
  