import Swal  from 'sweetalert2';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: "root",
})
export class MenssageService {
  /**
   * @description  create with sucess
   * @return {void}
   */
  createSuccess(): void {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "O item foi salvo com sucesso",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }

  /**
   * @description  delete with sucess
   * @return {void}
   */
  deleteSuccess(): void {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "O item foi excluido com sucesso",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }

  /**
   * @description  update with sucess
   * @return {void}
   */
  updateSuccess(): void {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "O item foi atualizado com sucesso",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }

  /**
   * @description  update with sucess
   * @return {void}
   */
  errorRest(): void {
    Swal.fire({
      position: "center",
      icon: "error",
      title: "Oops!, Tente novamente",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }

  /**
   * @description  error com menssage with sucess
   * @return {void}
   */
  errorHandler(error: string): void {
    Swal.fire({
      position: "center",
      icon: "error",
      title: error,
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }

  /**
   * @description  confirm delete
   * @return {Promise}
   */
  async confirmDelete(): Promise<any> {
    let canDelete = null;
    const buttons = Swal.mixin({
      customClass: {
        confirmButton: "msg-btn msg-btn-success",
        cancelButton: "msg-btn msg-btn-danger",
      },
      buttonsStyling: false,
    });

    await buttons
      .fire({
        title: "Tem certeza que deseja excluir?",
        text: "Não é possivel reverter isso!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.value) {
          canDelete = result.value;
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          buttons.fire("Cancelado", "Seu item está seguro :)", "error");
        }
      });

    return canDelete;
  }

  /**
   * @description  loading de e-mail
   * @return {void}
   */
  loadingMessage(): void {
    Swal.fire({
      title: "Aguarde!",
      html: "Por favor aguarde, enviando e-mail.",
      width: "50%",
      timer: 4000,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
      onClose: () => {
        setTimeout(() => {
          clearInterval();
        }, 4000);
      },
    }).then((result) => {
      if (result.dismiss === Swal.DismissReason.timer) {
        this.enviadoEmailSucesso();
      }
    });
  }

  /**
   * TODO Retorno Sucesso para envio de e-mail
   * @return {void}
   */
  enviadoEmailSucesso(): void {
    Swal.fire({
      icon: "success",
      title: "<strong> Parabéns! </strong>",
      timer: 10000,
      width: 600,
      padding: "3em",
      html:
        "Sua solicitação foi realizada com sucesso. " +
        " Aguarde a resposta no endereço cadastrado. Nosso prazo para envio é de até 30 dias. " +
        "Caso queira mais informações, entre em contato com os canais de atendimento: " +
        "166 ou ouvidoria@antt.gov.br",

      showCancelButton: false,
      showConfirmButton: false,
      timerProgressBar: true,
    });
  }

  /**
   * @description TODO Retorno Sucesso para criação de documento
   * @return {void}
   */
  criadoDocumentoSucesso(): void {
    Swal.fire({
      title: "Sucesso!",
      // text: 'Criado documento com Sucesso',
      // type: 'success',
      timer: 1000,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }

  /**
   * TODO Retorno error do email
   * @return {void}
   */
  erroEnviarEmail(): void {
    Swal.fire({
      icon: "error",
      title: "Oops!, Tente novamente",
      showConfirmButton: false,
      timer: 10000,
    });
  }

  /**
   * ! Limite de criação de documento alcançado
   * @return {void}
   */
  limiteDocumentoError(): void {
    Swal.fire({
      title: "Ops!",
      text: "Limite de criação de documento alcançado",
      // type: 'error',
    });
  }
}
