import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScmLoginComponent } from './scm-login.component';

describe('ScmLoginComponent', () => {
  let component: ScmLoginComponent;
  let fixture: ComponentFixture<ScmLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScmLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
