import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
// import {MatCardHarness} from '@angular/material/card/testing';
import {MatListModule} from '@angular/material/list';
import { ScmCadastroComponent } from 'src/client/scm/scm-cadastro/scm-cadastro.component';
import { ScmListagemComponent } from 'src/client/scm/scm-listagem/scm-listagem.component';
import { ScmDetalheComponent } from 'src/client/scm/scm-detalhe/scm-detalhe.component';
import { ScmComponent } from 'src/client/scm/scm.component';
import { ScmHeaderComponent } from 'src/client/scm/scm-header/scm-header.component';

const routes = [
  {
    path:"",
    Component: ScmListagemComponent
  },
  {
    path:"cadastro",
    Component: ScmCadastroComponent
  },
  {
    path:"listagem/editar/:id",
    Component: ScmCadastroComponent
  },
]

@NgModule({
  declarations: [
    AppComponent,
    ScmCadastroComponent,
    ScmListagemComponent,
    ScmDetalheComponent,
    ScmComponent,
    ScmHeaderComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatListModule,
    
 //   MatCardHarness
    
 //   routing

  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
