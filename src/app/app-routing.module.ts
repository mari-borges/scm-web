import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScmCadastroComponent } from 'src/client/scm/scm-cadastro/scm-cadastro.component';
import { ScmListagemComponent } from 'src/client/scm/scm-listagem/scm-listagem.component';


const routes: Routes = [
      {
        path: 'cadastro', component: ScmCadastroComponent
      },
      {
        path: '', component: ScmListagemComponent
      },
      {
        path: 'listagem/editar/:id', component: ScmCadastroComponent
      }
   //   {
   //     path: '', component: ScmHeaderComponent
   //   }

//  {
//    path: 'cadastro',
//    loadChildren: () =>
//        import('./scm/scm-cadastro/scm-cadastro.module').then((rota) => rota.ScmCadastroModule),
//  },
];

// export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
